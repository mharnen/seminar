//~~~~~~~~~~~~~~~~~~SLIDE 3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


function animate1(){
	cloud.style.visibility="visible";

}

function animate2(){
	cpu_cloud.style.visibility="visible";
	storage_cloud.style.visibility="visible";
}

function animate3(){
	cloud.style.visibility="hidden";
	cpu_cloud.style.visibility="hidden";
	storage_cloud.style.visibility="hidden";
}

function animate4(){
	clock1.style.visibility="visible";
	clock2.style.visibility="visible";
	clockAnimateID = setInterval(function() {
		$("#clock2").velocity("callout.flash");
	}, 2000);
}

function stop(){
	clearInterval(animate1ID);
	clearInterval(animate2ID);
	clearInterval(clockAnimateID);
	animateGear1.setAttribute('dur', '4s');
	node2.style.fill="white";
	node1.style.fill="white";
	node11.style.fill="white";
	clock1.style.visibility="hidden";
	clock2.style.visibility="hidden";
}

function movePacket1(){
	console.log("Time: " + packetInterval);
	$("#packet1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 21, translateY: 0}, packetInterval)
		.velocity({translateX: 48, translateY: 13}, packetInterval)
		.velocity({translateX: 80, translateY: 7}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function movePacket2(time){
	$("#packet2")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 17, translateY: 0}, packetInterval)
		.velocity({translateX: 47, translateY: 1}, packetInterval)
		.velocity({translateX: 73, translateY: -18}, packetInterval)
		.velocity({translateX: 83, translateY: -44}, packetInterval)
		.velocity({translateX: 83, translateY: -74}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
